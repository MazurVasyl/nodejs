import express from 'express';
const router = express.Router();

import Users from '../modules/db/tableUsers.mjs';
import { loggedUser } from '../middlewares/loggedUser.mjs';

router.get('/', loggedUser, async (req, res) => {
        const users = await Users.findAll({
        attributes:
            ['id', 'name', 'role']
    });

    res.render('report', {users: users});
});

export default router;
