import Sequelize from 'sequelize';
import mysql from 'mysql2';
import dotenv from 'dotenv';

dotenv.config({
    path: `.env.${process.env.NODE_ENV || 'local'}`
});

export const sequelize = new Sequelize(
    process.env['DATABASE'],
    process.env['USER'],
    process.env['PASSWORD'],
    {
        host: 'localhost',
        dialect: 'mysql'
    });
