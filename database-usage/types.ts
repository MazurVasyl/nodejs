import {Model, Optional} from "sequelize";

export interface DogsAttributes {
    id: number;
    name: string;
    owner: string;
    birth: string
}

export interface DogsCreationAttributes extends Optional<DogsAttributes, "id"> {}

export interface DogsInstance extends Model<DogsAttributes, DogsCreationAttributes>, DogsAttributes {}
