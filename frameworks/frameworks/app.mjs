import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import Emitter from 'events';
import readline from 'readline';
import fs from 'fs';

import loginRouter from './routes/login.mjs';
import usersRouter from './routes/users.mjs';
import reportRouter from './routes/report.mjs';
import { control, controlWriteStream } from './middlewares/control.mjs';

process.on('SIGTERM', () => process.exit(0));

const deleteControl = () => {
  controlWriteStream.close();

  try {
    fs.rmSync('./control.txt');
    console.log('control deleted');
  }
  catch (err) {
    console.log(err);
  }
}

const killProcess = () => process.kill(process.pid, 'SIGTERM');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const EventEmitter = new Emitter();

EventEmitter.on('off', async () => {
  deleteControl();
  await killProcess();
});

rl.on('line', input => {
  if (input === 'off') {
    EventEmitter.emit('off');
  }
});

const app = express();
const __dirname = path.resolve();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', true);
  res.setHeader('Access-Control-Allow-Methods', '*');
  res.setHeader('Access-Control-Allow-Headers', '*');

  next();
}));

app.use(control);

app.get('/', (request, response) => {
  response.redirect('login');
});

app.use('/login', loginRouter);
app.use('/users', usersRouter);
app.use('/report', reportRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
