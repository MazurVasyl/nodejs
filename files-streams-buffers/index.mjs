import http from 'http';
import fs from 'fs';
import readline from 'readline';

const hostname = '127.0.0.1';
const port = 3000;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let pathToFiles;
const arrayFileStatus = [];

const question = () => {
    return new Promise( res =>
        rl.question( 'Enter the path to files:', answer => res(answer))
    );
};

const writeStream = fs.createWriteStream('./text.txt');

const server = http.createServer((req, res) => {
    const readStream = fs.createReadStream('./text.txt');
    res.setHeader('Content-Type', 'app/txt');

    readStream.pipe(res);
});

server.listen(port, hostname);

const Path = async () => {
    pathToFiles = await question();

    fs.readdir(pathToFiles, (error, files) => {
        if(error) {
            console.log(error);
            return;
        }
        console.log(files);

        files.forEach((file, index) => {
            fs.stat(pathToFiles + '/' + file, (error, stats) => {
                if (error) {
                    console.log(error);
                    return;
                }

                arrayFileStatus[index] = Object.entries(stats).reduce((total, current) => {
                    total[current[0]] = current[1];
                    return total;
                }, {});
            });
        });
    });
};

function interval() {
    fs.readdir(pathToFiles, (error, files) => {
        if(error) {
            console.log(error);
            return;
        }

        files.forEach(file => {
            fs.stat(pathToFiles + '/' + file, (error, stats) => {
                if (error) {
                    console.log(error);
                    return;
                }

                const index = arrayFileStatus
                    .findIndex(value => value.birthtime.toISOString() === stats.birthtime.toISOString());

                const changedValues = Object.entries(stats)
                    .filter((value, i) =>
                        (value[1] instanceof Date)
                            ? value[1].toISOString() !== Object.values(arrayFileStatus[index])[i].toISOString()
                            : value[1] !== Object.values(arrayFileStatus[index])[i]
                    );

                if (changedValues.length > 0) {

                    changedValues.forEach(value => {
                        arrayFileStatus[index][value[0]] = value[1];
                        writeStream.write(JSON.stringify(value));
                    });

                    console.log(changedValues);
                }
            });
        });
    });
    console.log(new Date());
};

(async () => {
    await Path();
    setInterval(interval, 5000);
})();
