import http from 'http';
import fs from 'fs';
import readline from 'readline';

const hostname = '127.0.0.1';
const port = 3000;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const question = () => {
    return new Promise( res =>
        rl.question( 'Enter the path to files:', answer => res(answer))
    );
};
