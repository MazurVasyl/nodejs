import express from 'express';
const router = express.Router();

import Users from '../modules/db/tableUsers.mjs';
import { loggedUser } from '../middlewares/loggedUser.mjs';

router.post('/', loggedUser, async (req, res) => {
    if (!req.body.name || !req.body.role || !req.body.login || !req.body.password) {
        res.status(400).end('not all required data');
    }
    else {
        const newUser = await Users.create({
            name: req.body.name,
            role: req.body.role,
            login: req.body.login,
            password: req.body.password
        });

        res.send(newUser);
    }
});

router.get('/', loggedUser, async (req, res) => {
    const users = await Users.findAll({
        attributes:
            ['id', 'name', 'role', 'login']
    });
    res.json(users);
});

router.get('/:id', loggedUser, async (req, res) => {
    const user = await Users.findOne({
        where: {id: req.params.id},
        attributes: ['id', 'name', 'role', 'login']
    });

    if (user === null) {
        res.status(404).end('not found');
    }
    else {
        res.json(user);
    }
});

router.put('/:id', loggedUser, async (req, res) => {
    const user = await Users.findOne({
        where: {id: req.params.id},
        attributes: ['id', 'name', 'role', 'login']
    });

    if (user === null) {
        res.status(404).end('not found');
    }
    else {
        await Users.update({
                name: req.body.name,
                role: req.body.role,
                login: req.body.login,
                password: req.body.password
            },
            {
                where: {id: req.params.id},
            });

        const updatedUser = await Users.findOne({
            where: {id: req.params.id},
            attributes: ['id', 'name', 'role', 'login']
        });

        res.json(updatedUser);
    }
});

router.delete('/:id', loggedUser, async (req, res) => {
    const user = await Users.findOne({
        where: {id: req.params.id},
        attributes: ['id', 'name', 'role', 'login']
    });

    if (user === null) {
        res.status(404).end('not found');
    }
    else {
        await Users.destroy({
            where: {id: req.params.id}
        });

        res.send('user was deleted');
    }
});

export default router;
