import readline from 'readline';
import Emitter from 'events';
import os from 'os';
import http from 'http';
import {spawn} from 'child_process';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const EventEmitter = new Emitter();

process.on('SIGTERM', () => process.exit(0));

EventEmitter.on('os', () => {
    console.log('OS: '+ os.type());
    console.log('Version: '+ os.release());
    console.log('System memory: '+ Math.round(os.totalmem()/1073741824) +' GB');
});

EventEmitter.on('request', () => {

    const options = {
        host: 'w3schools.com',
        port: 80,
        path: '/',
        method: 'GET',
    };

    const request = http.request(options, response => response.on('data', data => console.log(`${data}`)));
    request.end();
});

EventEmitter.on('start', () => spawn('node', ['script.js'])
    .stdout.on('data', data => console.log(`${data}`)));

EventEmitter.on('finish', () => {
    console.log('app closed');
    process.kill(process.pid, 'SIGTERM');
});

rl.on('line', input => {

    switch (input) {
        case 'info about os':
            EventEmitter.emit('os');
            break;
        case 'request':
            EventEmitter.emit('request');
            break;
        case 'start':
            EventEmitter.emit('start');
            break;
        case 'finish app':
            EventEmitter.emit('finish');
            break;
    }
});

