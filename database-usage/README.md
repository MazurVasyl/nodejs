To start program enter "npm start". 
Then enter "node index.js" .
Then program will show few commands: "all" - to see whole table, "owner" - to select info by owner's name, 
  "dog" - to see info about any of dogs, "new" - to add new row to the table, 
  "off" - to finish program. 
