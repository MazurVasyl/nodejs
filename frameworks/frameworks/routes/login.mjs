import express from 'express';
const router = express.Router();

import Tokens from '../modules/db/tableTokens.mjs';
import Users from '../modules/db/tableUsers.mjs';

router.post('/', async (req, res) => {
    const admin = await Users.findOne({
        where: {
            login: req.body.login,
            password: req.body.password
        },
        attributes: ['id', 'role']
    });

    if (admin && admin.role === 'admin') {
        const userToken = 'abc';
        await Tokens.create({
            token: userToken,
            user_id: admin.id
        });

        res.json(userToken);
    }
    else {
        res.status(401).end();
    }
});

export default router;
