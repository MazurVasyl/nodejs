import fs from 'fs';

export const controlWriteStream = fs.createWriteStream('./control.txt');

export const control = function (req, res, next) {
    const date = 'Time: ' + new Date().toString() + '\n';
    const path = 'path: ' + req.originalUrl + '\n';
    const data = 'data: ' + JSON.stringify(req.body) + '\n';
    controlWriteStream.write(date);
    controlWriteStream.write(path);
    controlWriteStream.write(data);
    next();
}
