import {sequelize} from './connectingToDb.mjs';
import Sequelize from 'sequelize';

const Users = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    role: {
        type: Sequelize.STRING,
        allowNull: false
    },
    login: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

await sequelize.sync();

await Users.create({
    name: 'Vasyl',
    role: 'admin',
    login: 'vasyl',
    password: '1234'
});

await Users.create({
    name: 'Viktor',
    role: 'user',
    login: 'viktor',
    password: '12345'
});

await Users.create({
    name: 'Vova',
    role: 'user',
    login: 'vova',
    password: '12345'
});

await Users.create({
    name: 'Pavlo',
    role: 'user',
    login: 'pavlo',
    password: '12345'
});

export default Users;
