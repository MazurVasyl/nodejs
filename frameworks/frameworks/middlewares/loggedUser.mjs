import Tokens from '../modules/db/tableTokens.mjs';

export const loggedUser = async (req, res, next) => {

    const token = await Tokens.findOne({
        where: {
            token: req.headers.token
        }
    });

    if (token !== null) {
        next();
    }
    else {
        res.status(401).end();
    }
}
