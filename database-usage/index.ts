import {Sequelize, DataTypes} from 'sequelize';
import {DogsInstance} from './types';

import * as readline from 'readline';
import * as Emitter from 'events';
import {ReadLine} from 'readline';
import * as dotenv from 'dotenv';

process.on('SIGTERM', () => process.exit(0));

dotenv.config({
    path: `.env.${process.env.NODE_ENV || 'local'}`
});

const rl: ReadLine = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const EventEmitter: Emitter = new Emitter();

const sequelize: Sequelize = new Sequelize(
    process.env['DATABASE'],
    process.env['USER'],
    process.env['PASSWORD'],
    {
        host: 'localhost',
        dialect: 'mysql'
    });

const Dogs = sequelize.define<DogsInstance>('dogs', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    owner: {
        type: DataTypes.STRING,
        allowNull: false
    },
    birth: {
        type: DataTypes.DATE,
        allowNull: false
    }
});

sequelize.sync();

Dogs.create({
    name: 'Sandy',
    owner: 'Lennon',
    birth: '2015-01-03'
});

Dogs.create({
    name: 'Cookie',
    owner: 'Casey',
    birth: '2013-11-13'
});

Dogs.create({
    name: 'Charlie',
    owner: 'River',
    birth: '2016-05-21'
});

Dogs.create({
    name: 'San',
    owner: 'Len',
    birth: '2010-01-03'
});

Dogs.create({
    name: 'Char',
    owner: 'River',
    birth: '2019-06-01'
});

console.log('--> to select all from table enter "all"');
console.log('--> to select all dogs of a specific owner enter "owner"');
console.log('--> to select a specific dog enter "dog"');
console.log('--> to insert info about a new dog enter "new"');
console.log('--> to stop program enter "off"');

const ownerName = () => {
    rl.question('Enter the name of a specific owner:', async (answer: string) => {
        const result: DogsInstance[] = await Dogs.findAll({
            where: { owner: answer },
            attributes: ['id', 'name', 'birth']
        });
        console.log(`All dogs, whose owner is ${answer}:`, JSON.stringify(result, null, 2));
    });
}

const dogName = () => {
    rl.question('Enter the name of a specific dog:', async (answer: string) => {
        const result: DogsInstance[] = await Dogs.findAll({
            where: { name: answer },
            attributes: ['id', 'owner', 'birth']
        });
        console.log(`Info about the dog, called "${answer}":`, JSON.stringify(result, null, 2));
    });
}

const newDogName = () =>
    rl.question('Enter the name of a new dog:', (answer: string) => answer);

const newDogOwner = () =>
    rl.question('Enter the name of owner of a new dog:', (answer: string) => answer);

const birthDate = (answer1, answer2) => {
    rl.question('Enter the date (YYYY-MM-DD) of birth of a new dog:', async (answer3: string) => {

        await Dogs.create({
            name: answer1,
            owner: answer2,
            birth: answer3
        });

        const result: DogsInstance[] = await Dogs.findAll({
            where: { name: answer1 },
            attributes: ['id', 'name', 'owner', 'birth']
        });
        console.log(
            `Info about the new dog, called "${answer1}":`, JSON.stringify(result, null, 2));
    });
}

(async () => {
    EventEmitter.on('all', async () => {
        const result: DogsInstance[] = await Dogs.findAll({attributes: ['id', 'name', 'owner', 'birth']});
        console.log("All dogs:", JSON.stringify(result, null, 2));
    });

    EventEmitter.on('owner', async () => await ownerName());

    EventEmitter.on('dog', async () => await dogName());

    EventEmitter.on('new', async () => {
        const name = newDogName();
        const owner = newDogOwner();
        await birthDate(name, owner);
    });

    EventEmitter.on('off', async () => {

        await sequelize.drop();

        console.log('the table is dropped');
        console.log('program is stopped');
        process.kill(process.pid, 'SIGTERM');
    });
})();

rl.on('line', input => {
    switch (input) {
        case 'all':
            EventEmitter.emit('all');
            break;
        case 'owner':
            EventEmitter.emit('owner');
            break;
        case 'dog':
            EventEmitter.emit('dog');
            break;
        case 'new':
            EventEmitter.emit('new');
            break;
        case 'off':
            EventEmitter.emit('off');
            break;
    }
});
