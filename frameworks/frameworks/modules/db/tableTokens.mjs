import {sequelize} from './connectingToDb.mjs';
import Sequelize from 'sequelize';
import Users from './tableUsers.mjs';

const Tokens = sequelize.define('tokens', {
    token: {
        type: Sequelize.STRING,
        allowNull: false
    },
    user_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Users,
            key: 'id'
        },
        allowNull: false
    }
});

await sequelize.sync();

export default Tokens;
