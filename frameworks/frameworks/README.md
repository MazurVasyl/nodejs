To start the program enter in terminal "npm start".
The program generates in MySQL database "users" table called also "users" with few users and empty table "tokens".

Table "users":
{ name: 'Vasyl', role: 'admin', login: 'vasyl', password: '1234' };
{ name: 'Viktor', role: 'user', login: 'viktor', password: '12345' };
{ name: 'Vova', role: 'user', login: 'vova', password: '12345' };
{ name: 'Pavlo', role: 'user', login: 'pavlo', password: '12345' };

To login has to be written: login: 'vasyl', password: '1234', because only this user has the role "admin".
Then will be shown the list of users and buttons: 
1. "Edit" - to edit info about each user. 
2. "Delete" - to delete each user.
3. "Create new user" - to create a new user.
4. "Get a list of users" - to get a list with info about users.
5. "Find user by id" - to find a user by id.
6. "HTML-report about users" - to get HTML-report with info about users.
7. Button "Exit" - to go to login-page.

During the process of the program will be created file control.txt, where will be written info about each step, 
including Date, URL, and data of request.
